package com.example.toolboxtest.listeners

import com.example.toolboxtest.models.LoginResponse

interface LoginListener {
    fun onSuccess(response: LoginResponse)
    fun onFailure(codeResponse: Int)
}