package com.example.toolboxtest.listeners

import com.example.toolboxtest.models.Carousel

interface FetchMoviesListener {
    fun onStarted()
    fun onSuccess(response: List<Carousel>)
    fun onFailure(codeResponse: Int)
}