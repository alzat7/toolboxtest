package com.example.toolboxtest.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.toolboxtest.models.CarouselType
import com.example.toolboxtest.R
import com.example.toolboxtest.models.Movie
import com.example.toolboxtest.viewHolders.SectionViewHolder
import com.squareup.picasso.Picasso

class SectionAdapter(private val context: Context) : RecyclerView.Adapter<SectionViewHolder>() {

    private var movies = emptyList<Movie>()
    private var carouselType: String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SectionViewHolder {
        return if (viewType == CarouselType.POSTER.ordinal){
            val posterTypeView = LayoutInflater.from(context).inflate(R.layout.poster_item, parent, false)
            SectionViewHolder(posterTypeView)
        }else{
            val thumbTypeView = LayoutInflater.from(context).inflate(R.layout.thumb_item, parent, false)
            SectionViewHolder(thumbTypeView)
        }
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: SectionViewHolder, position: Int) {
        val currentItem = movies.elementAt(position)

        holder.movieTitle.text = currentItem.title
        Picasso.get().load(currentItem.imageUrl).into(holder.movieImage)
    }

    override fun getItemViewType(position: Int): Int {
        return if (carouselType == "thumb") CarouselType.THUMB.ordinal else CarouselType.POSTER.ordinal
    }

    fun setCarouselType(carouselType: String){
        this.carouselType = carouselType
        notifyDataSetChanged()
    }

    fun setMovies(movies: List<Movie>){
        this.movies = movies
        notifyDataSetChanged()
    }
}