package com.example.toolboxtest.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.toolboxtest.R
import com.example.toolboxtest.models.Carousel
import com.example.toolboxtest.viewHolders.MainViewHolder

class MainAdapter(private val context: Context) : RecyclerView.Adapter<MainViewHolder>() {

    private var sections = emptyList<Carousel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.horizontal_main_section, parent, false)
        return MainViewHolder(view)
    }

    override fun getItemCount(): Int {
        return sections.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val currentSection = sections.elementAt(position)

        val adapter = SectionAdapter(context)
        adapter.setCarouselType(currentSection.type)
        adapter.setMovies(currentSection.items)
        holder.sectionTitle?.text = currentSection.title
        holder.sectionRecyclerView?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        holder.sectionRecyclerView?.adapter = adapter
    }

    fun setSections(sections: List<Carousel>){
        this.sections = sections
        notifyDataSetChanged()
    }
}