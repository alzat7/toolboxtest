package com.example.toolboxtest.viewModels

import androidx.lifecycle.ViewModel
import com.example.toolboxtest.data.external.Coroutines
import com.example.toolboxtest.data.repositories.MoviesRepository
import com.example.toolboxtest.listeners.FetchMoviesListener

class FetchMoviesViewModel : ViewModel() {

    var fetchMoviesListener: FetchMoviesListener? = null
    var token: String? = null

    fun loadInBackground(){
        fetchMoviesListener?.onStarted()
        Coroutines.main {
            val response = MoviesRepository().fetchData(token!!)
            if (response.isSuccessful) {
                fetchMoviesListener?.onSuccess(response.body()!!)
            }else{
                fetchMoviesListener?.onFailure(response.code())
            }
        }
    }
}