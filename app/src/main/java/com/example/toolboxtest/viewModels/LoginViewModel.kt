package com.example.toolboxtest.viewModels

import androidx.lifecycle.ViewModel
import com.example.toolboxtest.data.external.Coroutines
import com.example.toolboxtest.data.repositories.AuthorizationRepository
import com.example.toolboxtest.listeners.LoginListener
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject

class LoginViewModel : ViewModel() {

    var loginListener: LoginListener? = null

    fun loadInBackground(){

        Coroutines.main {
            val json = JSONObject()
            json.put("sub", "ToolboxMobileTest")
            val requestBody: RequestBody = RequestBody.create(MediaType.parse("application/json"), json.toString())
            val response = AuthorizationRepository().login(requestBody)

            if (response.isSuccessful){
                response.body()?.let { loginListener?.onSuccess(it) }
            }else{
                loginListener?.onFailure(response.code())
            }
        }
    }
}