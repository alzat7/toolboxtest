package com.example.toolboxtest.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.example.toolboxtest.R
import com.example.toolboxtest.listeners.LoginListener
import com.example.toolboxtest.models.LoginResponse
import com.example.toolboxtest.viewModels.LoginViewModel

class SplashActivity : BaseActivity(), LoginListener {

    override var viewResource: Int = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startViewModel()
    }

    override fun startViewModel(){
        startRefreshTokenViewModel()
    }

    override fun onSuccess(response: LoginResponse) {
        super.onSuccess(response)

        val intent = Intent(this@SplashActivity, MainActivity::class.java)
        startActivity(intent)
    }
}
