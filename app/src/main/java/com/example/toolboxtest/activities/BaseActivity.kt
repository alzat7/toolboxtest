package com.example.toolboxtest.activities

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.toolboxtest.listeners.LoginListener
import com.example.toolboxtest.models.LoginResponse
import com.example.toolboxtest.viewModels.LoginViewModel

abstract class BaseActivity : AppCompatActivity(), LoginListener{
    abstract var viewResource : Int
    val tokenPreference = "tokenPreference"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(viewResource)
    }

    abstract fun startViewModel()

    protected fun getToken() : String{
        val preferences = getSharedPreferences(tokenPreference, Context.MODE_PRIVATE)
        return preferences.getString(tokenPreference, "").toString()
    }

    protected fun startRefreshTokenViewModel(){
        val viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        viewModel.loginListener = this
        viewModel.loadInBackground()
    }

    override fun onSuccess(response: LoginResponse) {
        val sharedPreferences = getSharedPreferences(tokenPreference, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(tokenPreference, "${response.type} ${response.token}")
        editor.apply()
    }

    override fun onFailure(codeResponse: Int) {
        Toast.makeText(this, "Error code $codeResponse", Toast.LENGTH_LONG).show()
    }
}