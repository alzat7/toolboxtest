package com.example.toolboxtest.activities

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.toolboxtest.R
import com.example.toolboxtest.adapters.MainAdapter
import com.example.toolboxtest.listeners.FetchMoviesListener
import com.example.toolboxtest.models.Carousel
import com.example.toolboxtest.viewModels.FetchMoviesViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), FetchMoviesListener {

    override var viewResource: Int = R.layout.activity_main
    private var adapter : MainAdapter? = null
    private val unauthorized: Int = 401

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startViewModel()
        createAdapter()
    }

    override fun startViewModel() {
        val viewModel = ViewModelProviders.of(this).get(FetchMoviesViewModel::class.java)
        viewModel.fetchMoviesListener = this
        viewModel.token = getToken()
        viewModel.loadInBackground()
    }

    private fun createAdapter(){
        adapter = MainAdapter(this)
    }

    override fun onStarted() {
        progress.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
    }

    override fun onSuccess(response: List<Carousel>) {
        progress.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE

        adapter?.setSections(response)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }

    override fun onFailure(codeResponse: Int) {
        if (codeResponse == unauthorized) {
            startRefreshTokenViewModel()
            return
        }

        Toast.makeText(this, "Error code $codeResponse", Toast.LENGTH_LONG).show()
    }
}
