package com.example.toolboxtest.viewHolders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.horizontal_main_section.view.*

class MainViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val sectionTitle = view.section_title
    val sectionRecyclerView = view.section_carousel
}