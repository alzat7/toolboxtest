package com.example.toolboxtest.viewHolders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.toolboxtest.R

class SectionViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val movieImage = view.findViewById<ImageView>(R.id.movie_image)
    val movieTitle = view.findViewById<TextView>(R.id.movie_title)
}