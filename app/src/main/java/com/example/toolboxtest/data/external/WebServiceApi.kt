package com.example.toolboxtest.data.external

import com.example.toolboxtest.models.Carousel
import com.example.toolboxtest.models.LoginResponse
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface WebServiceApi {

    @POST("/v1/mobile/auth")
    suspend fun authentication(
        @Body request: RequestBody
    ): Response<LoginResponse>

    @GET("/v1/mobile/data")
    suspend fun fetchData(
        @Header("authorization") token: String
    ): Response<List<Carousel>>

    companion object {
        operator fun invoke(): WebServiceApi {
            return Retrofit.Builder()
                .baseUrl("https://echo-serv.tbxnet.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WebServiceApi::class.java)
        }
    }
}