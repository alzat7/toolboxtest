package com.example.toolboxtest.data.repositories

import com.example.toolboxtest.data.external.WebServiceApi
import com.example.toolboxtest.models.Carousel
import retrofit2.Response

class MoviesRepository {

    suspend fun fetchData(header: String) : Response<List<Carousel>>{
        return WebServiceApi().fetchData(header)
    }
}