package com.example.toolboxtest.data.repositories

import com.example.toolboxtest.data.external.WebServiceApi
import com.example.toolboxtest.models.LoginResponse
import okhttp3.RequestBody
import retrofit2.Response

class AuthorizationRepository {

    suspend fun login(requestBody: RequestBody) : Response<LoginResponse> {
        return WebServiceApi().authentication(requestBody)
    }
}