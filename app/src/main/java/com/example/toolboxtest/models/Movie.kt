package com.example.toolboxtest.models

data class Movie(
    var title: String,
    var imageUrl: String,
    var videoUrl: String,
    var description: String
)