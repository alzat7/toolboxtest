package com.example.toolboxtest.models

enum class CarouselType {
    POSTER,
    THUMB
}