package com.example.toolboxtest.models

data class LoginResponse(
    var sub: String,
    var token: String,
    var type: String
)