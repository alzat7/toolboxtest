package com.example.toolboxtest.models

data class Carousel(
    var title: String,
    var type: String,
    var items: List<Movie>
)